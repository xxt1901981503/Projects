/*
 * AdjGraph.h
 *
 *  Created on: 2015年1月3日
 *      Author: Administrator
 */

#ifndef SRC_ADJGRAPH_H_
#define SRC_ADJGRAPH_H_

#include "AdjList.h"
#include "stdlib.h"

template<class T, class T2>
class AdjGraph {
public:
	AdjGraph(int arcNum, const int vexNum = 0) {
		this->arcNum = arcNum;
		this->vexNum = vexNum;
		adjList = new AdjList<T, T2> [vexNum];
	}
	~AdjGraph() {
		delete[] adjList;
	}
	void createGraph();
	void addEdge(T2 v, T2 w);
	void DFS(T2 x);
	void DFS(const int v, bool* visited);
	void components();
	void outPut();
	int locateVex(T2 v);
	void visit(T n);
//private:
	AdjList<T, T2>* adjList;
	int arcNum;
	int vexNum;
};

template<class T, class T2>
void AdjGraph<T, T2>::createGraph() {
	T2 v, w;
	cout << "输入 " << vexNum << "个顶点data：";
	for (int i = 0; i < vexNum; i++) {
		cin >> adjList[i].data;
		adjList[i].first = adjList[i].last = NULL;
	}
	cout << "输入 " << arcNum << "个弧data对：" << endl;
	for (int k = 0; k < arcNum; k++) {
		cin >> v >> w;
		addEdge(v, w);
	}
}

template<class T, class T2>
int AdjGraph<T, T2>::locateVex(T2 v) {
	for (int i = 0; i < vexNum; i++) {
		if (v == adjList[i].data) {
			return i;
		}
	}
	cout << "节点不存在，程序退出！";
	exit(1);
	return -1;
}

template<class T, class T2>
void AdjGraph<T, T2>::addEdge(T2 x, T2 y) {
	int v, w;
	v = locateVex(x);
	w = locateVex(y);
	adjList[v].insertBack(w);
}

template<class T, class T2>
void AdjGraph<T, T2>::DFS(T2 x) {
	int v = locateVex(x);
	bool* visited = new bool[vexNum];
	for (int i = 0; i < vexNum; i++) {
		visited[i] = false;
	}
	for (int k = 0; k < vexNum; k++)
		if (!visited[v])
			DFS(v, visited);
	delete[] visited;
}

template<class T, class T2>
void AdjGraph<T, T2>::DFS(const int v, bool* visited) {
	int w;
	LinkNode<T>* tmpNode;
	visited[v] = true;
	visit(v);
	for (tmpNode = adjList[v].first; tmpNode; tmpNode = tmpNode->next) {
		w = tmpNode->data;
		if (!visited[w])
			DFS(w, visited);
	}
}

template<class T, class T2>
void AdjGraph<T, T2>::visit(T n) {
	cout << " " << adjList[n].data;
}

template<class T, class T2>
void AdjGraph<T, T2>::outPut() {
	for (int i = 0; i < vexNum; i++) {
		cout << " " << adjList[i].data;
	}
	cout << endl;
}

template<class T, class T2>
void AdjGraph<T, T2>::components() {
	int count = 1;
	bool* visited = new bool[vexNum];
	for (int i = 0; i < vexNum; i++) {
		visited[i] = false;
	}
	for (int i = 0; i < vexNum; i++)
		if (!visited[i]) {
			cout << endl << "The " << count++ << "th Comonent: ";
			DFS(i, visited);
		}
	if (count == 2)
		cout << endl << "图是一个全连通图......" << endl;
	else
		cout << endl << "图由两个以上的连通分量组成......" << endl;
	delete[] visited;
}

#endif /* SRC_ADJGRAPH_H_ */
