/*
 * AdjList.h
 *
 *  Created on: 2015��1��3��
 *      Author: Administrator
 */

#ifndef SRC_AdjList_H_
#define SRC_AdjList_H_

#include "LinkNode.h"

template<class T, class T2>
class AdjList {
public:
	AdjList() :
			first(NULL), last(NULL) {

	}
	void insertBack(const T& e);
	void concatenate(LinkNode<T>& L);
//private:
	T2 data;
	LinkNode<T>* first, *last;
};

template<class T, class T2>
void AdjList<T, T2>::insertBack(const T& e) {
	if (first) {
		last->next = new LinkNode<T>(e, NULL);
		last = last->next;
	} else
		first = last = new LinkNode<T>(e, NULL);
}

template<class T, class T2>
void AdjList<T, T2>::concatenate(LinkNode<T>& L) {
	if (first) {
		last->next = L.first;
		last = L.last;
	} else {
		first = L.first;
		last = L.last;
	}
	L.first = L.last = NULL;
}

#endif /* SRC_AdjList_H_ */
