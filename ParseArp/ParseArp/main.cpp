
#include <iostream>
using namespace std;

#include<Winsock2.h>
#pragma comment(lib, "Ws2_32.lib")          //使用ntohs()函数,转换2B/4B的数据
#pragma comment(lib, "wpcap.lib")
#include "pcap.h"                           //需要另外导入，具体步骤见Winpcap使用说明

#include <fstream>
#include <iomanip>                          //格式化输出
#include <conio.h>                          //使用_getch()

struct arppkt {
    unsigned short hdtyp;                   //硬件地址
    unsigned short protyp;                  //协议类型
    unsigned char hdsize;                   //硬件地址长度
    unsigned char prosize;                  //协议地址长度
    unsigned short op;                      //操作值
    u_char smac[6];                         //源MAC地址
    u_char sip[4];                          //源IP地址
    u_char dmac[6];                         //目的MAC地址
    u_char dip[4];                          //目的IP地址
};

//自定义处理包函数
//pcap_pkthdr是winpcap加入的,在pcap.h中
//pkt_data表示MAC帧的起始位置
//out是输入流

void packet_handler(const pcap_pkthdr * header, const u_char * pkt_data, ostream & out) {
    int i;
    //从截获的数据帧中找到 arp 包头的位置
    arppkt * arph = (arppkt *)(pkt_data + 14);   //14为Ethernet帧头的长度

    //输出源IP地址
    for(int i = 0; i < 3; i++)
        out << int(arph->sip[i]) << '.';

    out.setf(ios::left);
    out << setw(3) << int(arph->sip[3]) << "  ";
    out.unsetf(ios::left);
    //输出源MAC地址
    char oldfillchar = out.fill('0');
    out.setf(ios::uppercase);                   //MAC地址以分开'-'的大写字符表示

    for(int i = 0; i < 5; i++)
        out << hex << setw(2) << int(arph->smac[i]) << '_';

    out << hex << setw(2) << int(arph->smac[5]) << "  ";
    out.fill(oldfillchar);
    //输出目的的IP地址
    out.unsetf(ios::hex | ios::uppercase);

    for(int i = 0; i < 3; i++)
        out << int(arph->dip[i]) << '.';

    out.setf(ios::left);
    out << setw(3) << int(arph->dip[3]) << " ";
    out.unsetf(ios::left);
    //输出目的MAC地址
    out.fill('0');
    out.setf(ios::uppercase);

    for(int i = 0; i < 5; i++)
        out << hex << setw(2) << int(arph->dmac[i]) << '_';

    out << hex << setw(2) << int(arph->dmac[5]) << "  ";
    out.fill(oldfillchar);
    out.unsetf(ios::hex | ios::uppercase);
    out << ntohs(arph->op) << " ";              //输出的操作类型,注意网络字节间的转换
    struct tm * ltime;                          //时间
    time_t local_tv_sec;
    local_tv_sec = header->ts.tv_sec;
    ltime = localtime(&local_tv_sec);
    //long long time = (long long)header->ts.tv_sec;
    //ltime = localtime(&time);
    //ltime = localtime(&header->ts.tv_sec);
    out.fill('0');
    out << ltime->tm_hour << ':' << setw(2) << ltime->tm_min << ':' << setw(2) << ltime->tm_sec;
    out.fill(oldfillchar);
    out << endl;
}

int main(int argc, char * argv[]) {
    if(argc != 2) {
        cout << "usage:arpparse logfilename" << endl;
        cout << "press anykey to continue." << endl;        //参数不对，进行提示
        _getch();
        return 0;
    }

    pcap_if_t * alldevs = NULL;                                  //网络设备结构
    pcap_if_t * d = NULL;
    pcap_t * adhandle = NULL;
    char errbuf[PCAP_ERRBUF_SIZE];                          //错误信息
    u_int netmask;                                          //子网掩码
    char packet_filter[] = "ether proto \\arp";         //过滤,选择ARP协议
    struct bpf_program fcode;
    struct pcap_pkthdr * header = NULL;
    const u_char * pkt_data = NULL;

    //获取本地网络设备列表
    if(pcap_findalldevs(&alldevs, errbuf) == -1) {
        fprintf(stderr, "Error in pcap_findalldevs_ex: %s\n", errbuf);
        return -1;
    }

    //打印列表
    for(d = alldevs; d; d = d->next) {
        //以混杂模式打开网卡,接受所有的帧
        if((adhandle = pcap_open_live(d->name, 65536, 1, 300, errbuf)) == NULL) {
            fprintf(stderr, "\nUnable to open the adapter. %s is not supported by WinPcap\n", d->name);
            pcap_freealldevs(alldevs);                      //释放设备列表
            return -1;
        }

        if(pcap_datalink(adhandle) == DLT_EN10MB && d->addresses != NULL)
            break;
    }

    if(d == NULL) {
        cout << "\nNO interfaces found! Make sure winpcap is installed.\n";
        return -1;
    }

    if(d->addresses != NULL)
        //获取接口第一个地址的掩码
        netmask = ((struct sockaddr_in *)(d->addresses->netmask))->sin_addr.S_un.S_addr;
    else
        //如果这个接口没有地址，那么我们假设这个接口在C类网络中
        netmask = 0xffffff;

    //调试过滤器，只捕获ARP包
    if(pcap_compile(adhandle, &fcode, packet_filter, 1, netmask) < 0) {
        fprintf(stderr, "\nUnable to compile the packet filter. Check the syntax.\n");
        pcap_freealldevs(alldevs);
        return -1;
    }

    //设置过滤器
    if(pcap_setfilter(adhandle, &fcode) < 0) {
        fprintf(stderr, "\nError setting the filter.\n");
        pcap_freealldevs(alldevs);
        return -1;
    }

    cout << "\t\tlistening on " << d->description << "..." << endl << endl;
    ofstream fout(argv[1], ios::app);                       //日志记录文件
    //加入日期记录
    time_t t;
    time(&t);
    fout.seekp(0, ios::end);

    if(int(fout.tellp()) != 0)
        fout << endl;

    fout << "\t\tARP request(1)/replay(2) on " << ctime(&t);
    cout << "sour IP Addr" << "   " << "Sour MAC Address"
         << "   " << "Des IP Addr" << "   " << "Des MAC Address"
         << "   " << "OP" << "   " << "Time" << endl;
    fout << "Sour IP Addr" << "  " << "Sour MAC Address"
         << "  " << "Des IP Addr" << "  " << "Des MAC Address"
         << "  " << "OP" << "  " << "Time" << endl;
    //释放设备列表
    pcap_freealldevs(alldevs);
    //开始捕获MAC帧
    int result;                                     //时间到返回结果

    while((result = pcap_next_ex(adhandle, &header, &pkt_data)) >= 0) {
        if(result == 0)
            continue;                               //超时时间到

        packet_handler(header, pkt_data, cout);     //解析ARP包，输出结果
        packet_handler(header, pkt_data, fout);     //输出到文件
    }

    if(result == -1) {
        printf("Error reading the packets: %s\n", pcap_geterr(adhandle));
        return -1;
    }

    return 0;
}