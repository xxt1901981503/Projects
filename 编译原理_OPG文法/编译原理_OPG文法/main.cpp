using namespace std;
#include <iostream>
#include <stdlib.h>
#include <fstream>
#define row 50
#define col 50
#define SIZE 50

//两个重要结构体的定义

//FIRSTVT表或LASTVT表中一个表项（A,a）结构体的初始化
typedef struct {
    char nonTerminal;           //非终结符
    char terminal;              //终结符
} StackElement;

//存放（A,a）的栈的初始化
typedef struct {
    StackElement *top;
    StackElement *bottom;
    int stacksize;
} stack;

//初始化（A,a）栈
void InitStack(stack &S) {
    S.bottom = new StackElement[SIZE];

    if(!S.bottom)
        cout << "存储空间分配失败！" << endl;

    S.top = S.bottom;
    S.stacksize = SIZE;
}

//判断（A,a）栈是否为空
bool ifEmpty(stack S) {
    if(S.top == S.bottom) return true;     //如果栈为空，则返回true
    else return false;               //否则不为空，返回false
}

//插入栈顶（A,a）元素
void Insert(stack &S, StackElement e) {
    if(S.top - S.bottom >= S.stacksize)
        cout << "栈已满，无法插入！" << endl;
    else {
        S.top->nonTerminal = e.nonTerminal;
        S.top->terminal = e.terminal;
        S.top++;
    }
}

//弹出栈顶（A,a）元素
StackElement Pop(stack &S) {
    StackElement e;
    e.nonTerminal = '\0';
    e.terminal = '\0';

    if(S.top == S.bottom) {
        cout << "栈为空，无法进行删除操作！" << endl;
        return e;
    } else {
        S.top--;
        e.nonTerminal = S.top->nonTerminal;
        e.terminal = S.top->terminal;
        return e;
    }
}

//终结符与非终结符的判断函数（布尔类型）
bool isTerminal(char c) {
    if(c >= 'A' && c <= 'Z') return false;  //非终结符返回false
    else return true;               //终结符返回true
}

//判断非终结符在first表中是否已存在
bool ItemJud(char first[][col], int frist_len, char C) {
    for(int i = 0; i < frist_len; i++) {
        if(first[i][0] == C)
            return true;     //如果first表中已存在此非终结符，则返回true
    }

    return false;
}

//读文件函数
int readFile(char sen[][col]) {
    int i;
    char addr[50];
    cout << "请输入要读文件的地址（\\用\\\\表示）：" << endl;
    cin >> addr;
    ifstream fin;
    fin.open(addr, ios::in);

    if(!fin) {
        cout << "Cannot open file!\n" << endl;
    }

    for(i = 0; !fin.eof(); i++) {
        fin >> sen[i];
        cout << sen[i] << endl;
    }

    return i;
}

//FIRSTVT表和LASTVT表中表项（非终结符）的初始化
void ItemInit(char sen[][col], char first[][col], char last[][col], int sen_len, int &frist_len) {
    int i;
    frist_len = 1;
    first[0][0] = sen[0][0];
    last[0][0] = sen[0][0];

    for(i = 1; i < sen_len; i++) {
        if(isTerminal(sen[i][0]) == false && ItemJud(first, frist_len, sen[i][0]) == false) {  //k是当前first和last表的长度
            first[frist_len][0] = sen[i][0];
            last[frist_len][0] = sen[i][0];
            frist_len++;
        }
    }
}



void FIRSTVT(char sen[][col], char first[][col], int sen_len, int frist_len) {    // frist_len 是 first 表的行数 sen_len是产生式的个数
    StackElement DFS, record[SIZE];
    stack Operator;       //创建存放（A，a）的栈
    InitStack(Operator);
    int i, j, r = 0;

    for(i = 0; i < sen_len; i++) {         //第一次扫描，将能直接得出的first（A，a）放进栈中
        for(j = 3; sen[i][j] != '\0'; j++) {
            if(isTerminal(sen[i][j]) == true) {                //遇到的第一个终结符压入
                int exist = 0;
                DFS.nonTerminal = sen[i][0];
                DFS.terminal = sen[i][j];

                for(int i1 = 0; i < r; i++) {
                    if(record[i1].nonTerminal == sen[i][0] && record[i1].terminal == sen[i][j]) {
                        exist = 1;
                        break;
                    }
                }

                record[r].nonTerminal = sen[i][0];
                record[r].terminal = sen[i][j];

                if(exist == 0) {
                    Insert(Operator, DFS);//A-aB A-aC (A,a)压栈两次?
                    record[r].nonTerminal = sen[i][0];
                    record[r].terminal = sen[i][j];
                    r++;
                }

                break;
            }
        }
    }

    int location[col];        //辅助数组，用来记录first表中放入终结符的位置

    for(i = 0; i < frist_len; i++)
        location[i] = 1;

    while(!ifEmpty(Operator)) {
        int exist = 0;        //标志位，记录即将入栈的元素是否已经存在
        StackElement IDElement, DElement;
        DElement = Pop(Operator);    //弹出栈顶元素

        for(i = 0; i < frist_len; i++) {
            if(first[i][0] == DElement.nonTerminal) {
                int n = location[i];
                first[i][n] = DElement.terminal;     //将终结符填入相应的first表中
                location[i]++;
                break;
            }
        }

        for(j = 0; j < sen_len; j++) {
            if(sen[j][3] == DElement.nonTerminal) {   //找出能推出当前非终结符的产生式的左部
                IDElement.nonTerminal = sen[j][0];
                IDElement.terminal = DElement.terminal;

                //判断将要放进栈里的元素曾经是否出现过，若没有，才压入栈
                for(int r0 = 0; r0 < r; r0++) {  //r记录record数组中的元素个数
                    if(record[r0].nonTerminal == IDElement.nonTerminal && record[r0].terminal == IDElement.terminal) {
                        exist = 1;
                        break;
                    }
                }

                if(exist == 0) {
                    Insert(Operator, IDElement);
                    record[r].nonTerminal = IDElement.nonTerminal;
                    record[r].terminal = IDElement.terminal;
                    r++;
                }
            }//if
        }//for
    }//while
}


void LASTVT(char sen[][col], char last[][col], int sen_len, int frist_len) {    //firstvt表与lastvt表行数一样 first_len表示last 表的行数
    int i, j, i1, j1;
    char c, record[row][col] = { '\0' };

    for(i = 0; i < sen_len; i++) {
        for(j = 0; sen[i][j] != '\0'; j++) {
            record[i][j] = sen[i][j];
        }

        j = j - 1;

        for(i1 = 3, j1 = j; i1 < j1; i1++, j1--) {             //做翻转，就可以用求first的方法求last
            c = record[i][i1];
            record[i][i1] = record[i][j1];
            record[i][j1] = c;
        }
    }//for

    FIRSTVT(record, last, sen_len, frist_len);
}

//判断非终结符在terminal表中是否已存在
bool TermTableJud(char terminal[col], int terminal_len, char C) {
    for(int i = 0; i < terminal_len; i++) {
        if(terminal[i] == C)
            return true;     //如果first表中已存在此非终结符，则返回1
    }

    return false;
}


//构造算符优先关系表
bool isPRT(char sen[][col], char first[][col], char last[][col], char opTable[][col], int sen_len, int first_len, int &opTable_len) {
    int i, j, terminal_len = 0;
    int i2, i3, opr, opc;
    char c1, c2, c3;
    char terminal[SIZE] = { '\0' };

    for(i = 0; i < sen_len; i++) {      //一维数组terminal记录关系表中存在的所有终结符
        for(j = 3; sen[i][j] != '\0'; j++) {
            if(isTerminal(sen[i][j]) == true)
                if(TermTableJud(terminal, terminal_len, sen[i][j]) == false) {  //terminal_len记录terminal表的长度
                    terminal[terminal_len] = sen[i][j];
                    terminal_len++;
                }
        }
    }                           //得到终结符表

    for(i = 0; i < terminal_len + 1; i++)      //给优先关系表赋初值，都等于空
        for(j = 0; j < terminal_len + 1; j++)
            opTable[i][j] = ' ';

    for(i = 1; i < terminal_len + 1; i++) {     //设置优先关系表的表头
        opTable[i][0] = terminal[i - 1];
        opTable[0][i] = terminal[i - 1];
    }

    for(i = 0; i < sen_len; i++) {   //找等于关系
        for(j = 5; sen[i][j] != '\0'; j++) {
            if(isTerminal(sen[i][j - 2]) == true && isTerminal(sen[i][j - 1]) == false && isTerminal(sen[i][j]) == true) {
                c1 = sen[i][j - 2];
                c2 = sen[i][j];

                for(opr = 1; opr < terminal_len + 1; opr++) {     //在opTable表中找到该存入的行标opr
                    if(opTable[opr][0] == c1)
                        break;
                }

                for(opc = 1; opc < terminal_len + 1; opc++) {       //在opTable表中找到该存入的列标opc
                    if(opTable[0][opc] == c2)
                        break;
                }

                if(opTable[opr][opc] != ' ') {
                    cout << "不是算符优先文法！" << endl;
                    return false;
                } else {
                    opTable[opr][opc] = '=';
                }
            }//if
        }//for(j)

        for(j = 4; sen[i][j] != '\0'; j++) {
            if(isTerminal(sen[i][j - 1]) == true && isTerminal(sen[i][j]) == true) {
                c1 = sen[i][j - 1];
                c2 = sen[i][j];

                for(opr = 1; opr < terminal_len + 1; opr++) {     //在opTable表中找到该存入的行标opr
                    if(opTable[opr][0] == c1)
                        break;
                }

                for(opc = 1; opc < terminal_len + 1; opc++) {       //在opTable表中找到该存入的列标j2
                    if(opTable[0][opc] == c2)
                        break;
                }

                if(opTable[opr][opc] != ' ') {
                    cout << "不是算符优先文法！" << endl;
                    return false;
                } else {
                    opTable[opr][opc] = '=';
                }
            }
        }
    }//for(i)

    for(i = 0; i < sen_len; i++) {   //找小于关系
        for(j = 3; sen[i][j] != '\0'; j++) {
            if(isTerminal(sen[i][j]) == true && isTerminal(sen[i][j + 1]) == false) {
                c1 = sen[i][j];        //c1记录终结符
                c2 = sen[i][j + 1];        //c2记录非终结符

                for(opr = 1; opr < terminal_len + 1; opr++) {     //在opTable表中找到该存入的行标opr
                    if(opTable[opr][0] == c1)
                        break;
                }

                for(opc = 0; opc < first_len; opc++) {       //找出非终结符在first表中的列标opc
                    if(first[opc][0] == c2)
                        break;
                }

                for(i2 = 1; first[opc][i2] != '\0'; i2++) {
                    c3 = first[opc][i2];

                    for(i3 = 1; i3 < terminal_len + 1; i3++)
                        if(opTable[0][i3] == c3) {
                            if(opTable[opr][i3] != ' ') {
                                cout << "不是算符优先文法！" << endl;
                                return false;
                            } else {
                                opTable[opr][i3] = '<';
                            }

                            break;
                        }
                }
            }//if
        }//for(j)
    }//for(i)

    for(i = 0; i < sen_len; i++) {   //找大于关系
        for(j = 3; sen[i][j] != '\0'; j++) {
            if(isTerminal(sen[i][j]) == false && sen[i][j + 1] != '\0' && isTerminal(sen[i][j + 1]) == true) {
                c1 = sen[i][j];        //c1记录非终结符
                c2 = sen[i][j + 1];        //c2记录终结符

                for(opr = 1; opr < terminal_len + 1; opr++) {     //在opTable表中找到该存入的列标j1
                    if(opTable[0][opr] == c2)
                        break;
                }

                for(opc = 0; opc < first_len; opc++) {       //找出非终结符在last表中的行标j2
                    if(last[opc][0] == c1)
                        break;
                }

                for(i2 = 1; last[opc][i2] != '\0'; i2++) {
                    c3 = last[opc][i2];

                    for(i3 = 1; i3 < terminal_len + 1; i3++)
                        if(opTable[i3][0] == c3) {
                            if(opTable[i3][opr] != ' ') {
                                cout << "不是算符优先文法！" << endl;
                                return false;
                            } else {
                                opTable[i3][opr] = '>';
                            }

                            break;
                        }
                }
            }//if
        }//for(j)
    }//for(i)

    opTable_len = terminal_len + 1;
    return true;
}

//判断两算符优先关系并给出类型供构造分析表
int getPR(char c1, char c2, char opTable[][col], int opTable_len) {
    int i, j;

    for(i = 1; i < opTable_len; i++) {
        if(opTable[i][0] == c1)
            break;
    }

    for(j = 1; j < opTable_len; j++) {
        if(opTable[0][j] == c2)
            break;
    }

    if(opTable[i][j] == '<')
        return 1;
    else if(opTable[i][j] == '=')
        return 2;
    else if(opTable[i][j] == '>')
        return 3;
    else return 4;
}

//判断输入串是不是算符文法
bool isOG(char sen[][col], int sen_len) {
    int i, j;

    for(i = 0; i < sen_len; i++) {
        for(j = 4; sen[i][j] != '\0'; j++) {
            if(isTerminal(sen[i][j - 1]) == false && isTerminal(sen[i][j]) == false) {
                cout << "输入的不是算符文法!" << endl;
                return false;
            }
        }
    }

    return true;
}

//开始分析输入串
void inputAnalyse(char opTable[][col], char string[col], int opTable_len) {  //opTable_len是opTable表的长度
    char a, Q, S[SIZE] = { '\0' }; //S是分析栈
    char cho1, cho2;
    int i, j, relation;
    int k = 0;
    S[k] = '#';
    cho2 = 'y';
    cout << "\t\t\t\t分析过程如下：" << endl;
    cout << "-----------------------------------------------------------------------------" << endl;
    //cout<<"  栈\t当前字符\t优先关系\t移进或规约"<<endl;
    //cout<<"分析过程如下："<<endl;
    cout.width(10);
    cout << "栈";
    cout.width(15);
    cout << "当前字符";
    cout.width(20);
    cout << "剩余符号串";
    cout.width(15);
    cout << "优先关系";
    cout.width(15);
    cout << "移进或规约" << endl;
    char* p;
    p = string;
    p++;

    for(i = 0; string[i] != '\0' && cho2 == 'y'; i++, p++) {
        a = string[i];              //读入当前字符
        cho1 = 'n';                               //可否接受

        while(cho1 == 'n') {
            if(isTerminal(S[k]) == true)   j = k;             //规约使栈内有非终结符
            else  j = k - 1;

            relation = getPR(S[j], a, opTable, opTable_len);

            if(relation == 3) {           //S[j]>a
                //cout<<" "<<S<<"\t"<<"   "<<a<<"\t\t   >\t\t";
                cout.setf(ios::left);
                cout.width(10);
                cout << S;
                cout.unsetf(ios::left);
                cout.width(15);
                cout << a;
                cout.width(20);
                cout << p;
                cout.width(15);
                cout << ">";

                do {
                    Q = S[j];

                    if(isTerminal(S[j - 1]) == true)  j = j - 1;
                    else j = j - 2;
                } while(getPR(S[j], Q, opTable, opTable_len) != 1);

                //cout<<"    "<<S[j]<<"归约"<<endl;
                cout.width(11);
                cout << S[j];
                cout.width(4);
                cout << "归约" << endl;
                k = j + 1;
                S[k] = 'N';

                for(int l = k + 1; l < SIZE; l++) {
                    S[l] = '\0';
                }
            } else if(relation == 1) {     //S[j]<a
                cho1 = 'y';
                //cout<<" "<<S<<"\t"<<"   "<<a<<"\t\t   <\t\t    移进"<<endl;
                cout.setf(ios::left);
                cout.width(10);
                cout << S;
                cout.unsetf(ios::left);
                cout.width(15);
                cout << a;
                cout.width(20);
                cout << p;
                cout.width(15);
                cout << "<";
                cout.width(15);
                cout << "移进" << endl;
                k = k + 1;
                S[k] = a;
            } else if(relation == 2) {       //S[j]=a
                cho1 = 'y';
                //cout<<" "<<S<<"\t"<<"   "<<a<<"\t\t   =\t\t";
                cout.setf(ios::left);
                cout.width(10);
                cout << S;
                cout.unsetf(ios::left);
                cout.width(15);
                cout << a;
                cout.width(20);
                cout << p;
                cout.width(15);
                cout << "=";
                cout.width(15);

                if(getPR(S[j], '#', opTable, opTable_len) == 2) {
                    cout << "    接受" << endl;
                    break;
                } else {
                    cout << "    移进" << endl;
                    k = k + 1;
                    S[k] = a;
                }
            } else {
                cho1 = 'y';
                //cout<<" "<<S<<"\t"<<a<<"\t\t出错"<<endl;
                cout << S << "     " << a << "              出错" << endl;
                cout << "对不起！字符串有错！" << endl;
                cho2 = 'n';
                break;
            }
        }//while
    }//for
}//

//
void main() {
    char choice = 'y';

    while(choice == 'y') {
        system("cls");
        char sen[row][col] = { '\0' }, first[row][col] = { '\0' }, last[row][col] = { '\0' }, opTable[row][col] = { '\0' };
        char string[col];
        int i, k, p, q;
        p = readFile(sen);

        if(isOG(sen, p) == true) {
            ItemInit(sen, first, last, p, q);     //j记录FIRSTVT和LASTVT表的长度
            FIRSTVT(sen, first, p, q);
            cout << "\t\t各非终结符FIRSTVT集" << endl;
            cout << "------------------------------------------------" << endl;

            for(i = 0; i < p; i++) {
                for(int h = 0; h < col; h++)
                    cout << first[i][h] << "  ";

                cout << endl;
            }

            //  cout<<endl;
            LASTVT(sen, last, p, q);
            cout << "\t\t各非终结符LASTVT集" << endl;
            cout << "-------------------------------------------------" << endl;

            for(i = 0; i < p; i++) {
                for(int h = 0; h < col; h++)
                    cout << last[i][h] << "  ";

                cout << endl;
            }

            //  cout<<endl;

            if(isPRT(sen, first, last, opTable, p, q, k) == true) {  //k记录opTable表的长度
                cout << "\t\t优先关系表：" << endl;
                cout << "----------------------------------------------" << endl;

                for(i = 0; i < k; i++) {
                    for(int h = 0; h < col; h++)
                        cout << opTable[i][h] << " ";

                    cout << endl;
                }

                //  cout<<endl;
                cout << "请输入要分析的串,以#结束:" << endl;
                cin >> string;
                inputAnalyse(opTable, string, k);    //k是opTable表的长度
            }
        }

        cout << "是否继续？ y/n" << endl;
        cin >> choice;
    }
}