/*
 * LinkQueue.h
 *
 *  Created on: 2015年1月2日
 *      Author: Administrator
 */

#ifndef SRC_LINKQUEUE_H_
#define SRC_LINKQUEUE_H_

#include<iostream>
using namespace std;

#include "LinkNode.h"

template<class T> class LinkQueue;

template<class T>
class LinkQueue {
public:
	LinkQueue() {
		len = 0;
		front = rear = new LinkNode<T>;
//		front->next = NULL;
	}
	void push(const T& e);
	void pop();
	bool isEmpty();
	void OutPut();
	void destroy();
	int getLen();
private:
	int len;
	LinkNode<T>* front;
	LinkNode<T>* rear;
};

template<class T>
void LinkQueue<T>::push(const T& e) {
//	rear->data = e;
	LinkNode<T>* tmpNode = new LinkNode<T>(e, NULL);
//	LinkNode<T>* tmpNode = new LinkNode<T>;
	rear->next = tmpNode;
	rear = tmpNode;
	len++;
//	delete tmpNode;
}

template<class T>
bool LinkQueue<T>::isEmpty() {
	return front == rear;
}

template<class T>
void LinkQueue<T>::pop() {
	if (isEmpty()) {
		cout << "队列空！";
		return;
	}
	LinkNode<T>* tmpNode = front->next;
//	LinkNode<T>* tmpNode = front;
	front->next = tmpNode->next;
//	front = front->next;

//	一般情况下，删除对头元素仅需修改头结点中的指针，
//	但队列只有一个节点时，出队列将丢失队尾指针，这种情况下，只需修改队尾指针，令它指向头节点
	if (rear == tmpNode)
		rear = front;
	len--;
	delete tmpNode;
}

template<class T>
void LinkQueue<T>::destroy() {
	while (front) {
		rear = front->next;
		delete front;
		front = rear;
	}
}

template<class T>
int LinkQueue<T>::getLen() {
	return len;
}

template<class T>
void LinkQueue<T>::OutPut() {
	if (isEmpty()) {
		cout << "队列空！";
		return;
	}
	LinkNode<T>* tmpNode = front->next;
	while (tmpNode != rear->next) {
		tmpNode->outPut();
		tmpNode = tmpNode->next;
	}
//	delete tmpNode;
}

#endif /* SRC_LINKQUEUE_H_ */
