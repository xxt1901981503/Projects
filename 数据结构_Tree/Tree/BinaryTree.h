/*
 * BinaryTree.h
 *
 *  Created on: 2014��12��30��
 *      Author: Administrator
 */

#ifndef SRC_TREE_H_
#define SRC_TREE_H_

#include<iostream>
#include "TreeNode.h"
using namespace std;

template<class T> class BinaryTree;

template<class T>
class BinaryTree {
public:
	BinaryTree() {
		TreeNode<T>* d = new TreeNode<T>(12, NULL, NULL);
		TreeNode<T>* e = new TreeNode<T>(10, NULL, NULL);
		TreeNode<T>* f = new TreeNode<T>(22, NULL, NULL);
		TreeNode<T>* c = new TreeNode<T>(25, NULL, f);
		TreeNode<T>* b = new TreeNode<T>(15, d, e);
		TreeNode<T>* a = new TreeNode<T>(20, b, c);
		root = a;
	}
	~BinaryTree() {
	}
	;
	void preOrder();
	void preOrder(TreeNode<T>* currentNode);
	void inOrder();
	void inOrder(TreeNode<T>* currentNode);
	void postOrder();
	void postOrder(TreeNode<T>* currentNode);
	void visit(TreeNode<T> *v);
private:
	TreeNode<T> *root;
};

template<class T>
void BinaryTree<T>::preOrder() {
	preOrder(root);
}
template<class T>
void BinaryTree<T>::preOrder(TreeNode<T>* currentNode) {
	if (currentNode) {
		visit(currentNode);
		preOrder(currentNode->lChild);
		preOrder(currentNode->rChild);
	}
}

template<class T>
void BinaryTree<T>::inOrder() {
	inOrder(root);
}
template<class T>
void BinaryTree<T>::inOrder(TreeNode<T>* currentNode) {
	if (currentNode) {
		inOrder(currentNode->lChild);
		visit(currentNode);
		inOrder(currentNode->rChild);
	}
}

template<class T>
void BinaryTree<T>::postOrder() {
	postOrder(root);
}
template<class T>
void BinaryTree<T>::postOrder(TreeNode<T>* currentNode) {
	if (currentNode) {
		postOrder(currentNode->lChild);
		postOrder(currentNode->rChild);
		visit(currentNode);
	}
}

template<class T>
void BinaryTree<T>::visit(TreeNode<T>* v) {
	cout << v->data << " ";
}

#endif /* SRC_TREE_H_ */
