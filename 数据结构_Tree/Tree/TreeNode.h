/*
 * TreeNode.h
 *
 *  Created on: 2014��12��30��
 *      Author: Administrator
 */

#ifndef SRC_TREENODE_H_
#define SRC_TREENODE_H_

template<class T>
class TreeNode {
public:
	TreeNode(T data, TreeNode<T>* lChild, TreeNode<T>* rChild) {
		this->data = data;
		this->lChild = lChild;
		this->rChild = rChild;
	}
//private:
	T data;
	TreeNode<T>* lChild;
	TreeNode<T>* rChild;
};

#endif /* SRC_TREENODE_H_ */
