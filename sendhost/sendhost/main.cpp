#pragma pack(4)
#pragma comment (lib,"Ws2_32.lib")
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sys/timeb.h>
#include <time.h>
#include<winbase.h>
typedef struct iphdr{
	unsigned int headlen:4;
	unsigned int version:4;
	unsigned char tos;
	unsigned short totallen;
	unsigned short id;
	unsigned short falg;
	unsigned char ttl;
	unsigned char prot;
	unsigned short checksum;
	unsigned int sourceIP;
	unsigned int destIP;
}IpHeader;

typedef struct icmphdr{
	BYTE type;
	BYTE code;
	USHORT checksum;
	USHORT id;
	USHORT seg;
}IcmpHeader;


#define ICMP_RCHO 8
#define ICMP_RCHO_REPLY 0
#define	ICMP_MIN 8
#define STATUS_FAILED 0xFFFF
#define	DEF_PACKET_SIZE 32
#define MAX_PACKET 1024
#define MAX_PING_PACKET_SIZE (MAX_PACKET+sizeof(IpHeader))

void fill_icmp_data(char *,int);
USHORT checksum(USHORT *,int);
void decode_resp(char *,int,struct sockaddr_in *);
DWORD WINAPI FindIP(LPVOID pIPAddrTemp);
WSADATA wsaData;
SOCKET sockRaw;
struct sockaddr_in dest,from,end;
int fromlen =sizeof(from);
char *recvbuf=new char[MAX_PING_PACKET_SIZE];
unsigned int addr=0;
long ThreadNumCounter=0,ThreadNumLimit=20;
long *aa=&ThreadNumCounter;
void main(int argc,char *argv[]){
	if(argc!=3)
	{
		std::cout<<"输入格式错误: start_ip end_ip"<<std::endl;
		return;
	}

	if(WSAStartup(MAKEWORD(2,1),&wsaData)!=0)
	{
		std::cout<<"WASStartup failed"<<GetLastError()<<std::endl;
		ExitProcess(STATUS_FAILED);
	}
	sockRaw=WSASocket(AF_INET,SOCK_RAW,IPPROTO_ICMP,NULL,0,WSA_FLAG_OVERLAPPED);
	if(sockRaw==INVALID_SOCKET)
	{
		std::cout<<"WASSocketet() falied"<<WSAGetLastError()<<std::endl;
		ExitProcess(STATUS_FAILED);
	}
	int timeout=1000;
	int bread=setsockopt(sockRaw,SOL_SOCKET,SO_RCVTIMEO,(char *)&timeout,sizeof(timeout));
	if(bread==SOCKET_ERROR)
	{
		std::cout<<"FAILED TO SEY RECV TIMEOUT"<<WSAGetLastError()<<std::endl;
		ExitProcess(STATUS_FAILED);

	}
	 timeout=1000;
	 bread=setsockopt(sockRaw,SOL_SOCKET,SO_SNDTIMEO,(char *)&timeout,sizeof(timeout));
	 if(bread==SOCKET_ERROR)
	{
		std::cout<<"FAILED TO SEY RECV TIMEOUT"<<WSAGetLastError()<<std::endl;
		ExitProcess(STATUS_FAILED);
	}
	memset(&dest,0,sizeof(dest));
	unsigned long startIP,endIP;
	dest.sin_family=AF_INET;
	dest.sin_addr.s_addr=inet_addr(argv[1]);
	startIP=inet_addr(argv[1]);
	end.sin_family=AF_INET;
	end.sin_addr.s_addr=inet_addr(argv[2]);
	endIP=inet_addr(argv[2]);
	HANDLE hThread;
	while(htonl(startIP)<=htonl(endIP))
	{
		if(ThreadNumCounter>ThreadNumLimit)
		{
			Sleep(5000);
			continue;
		}
		DWORD ThreadID;
		sockaddr_in *pIPAddrTemp=new (sockaddr_in);
		if(!pIPAddrTemp)
		{
			std::cout<<"memory alloc failed"<<std::endl;
			return ;
		}
		*pIPAddrTemp=dest;
		clock_t start;
		start=clock();
	hThread=CreateThread(NULL,NULL,FindIP,(LPVOID)pIPAddrTemp,NULL,&ThreadID);
		long i=60000000L;
		while(i--);
		TerminateThread(hThread,0);
		InterlockedDecrement(aa);
		memset(&from,0,sizeof(from));
		startIP=htonl(htonl(startIP)+1);
		dest.sin_addr.s_addr=startIP;
	}
		while(ThreadNumCounter!=0)
		{
			Sleep(2000);
			return;
			std::cout<<"error"<<std::endl;
		}
}
void fill_icmp_data(char *icmp_data,int datasize)
	{
		IcmpHeader *icmp_hdr;
		char *datapart;
		icmp_hdr=(IcmpHeader*)icmp_data;
		icmp_hdr->type=ICMP_RCHO;
		icmp_hdr->id=(USHORT)GetCurrentThreadId();
		datapart=icmp_data+sizeof(IcmpHeader);
		memset(datapart,'A',datasize-sizeof(IcmpHeader));
	}
    int i=0;
	void decode_resp(char *buf,int bytes,struct sockaddr_in *from){
		IpHeader *iphdr;
		IcmpHeader *icmphdr;
		unsigned short iphdrlen;
		iphdr=(IpHeader*) buf;
		iphdrlen=iphdr->headlen*4;
		icmphdr=(IcmpHeader *)(buf+iphdrlen);
		if(bytes<iphdrlen+ICMP_MIN)return;
		if(icmphdr->type!=ICMP_RCHO_REPLY)return;
		if(icmphdr->id!=(USHORT)GetCurrentThreadId())return;
		std::cout<<"\n活动主机"<<++i<<": "<<inet_ntoa(from->sin_addr)<<"\n"<<std::endl;
	}

USHORT checksum(USHORT *buffer,int size)
	{
		unsigned long cksum=0;
		while(size>1)
		{
			cksum+=*buffer++;
			size-=sizeof(USHORT);
		}
		if(size)
		{
			cksum+=*(UCHAR*)buffer;
		}
		cksum=(cksum>>16)+(cksum& 0xffff);
		cksum+=(cksum>>16);
		return (USHORT)(~cksum);
	}
DWORD WINAPI FindIP(LPVOID pIPAddrTemp)
	{
		InterlockedIncrement(aa);
		char icmp_data[MAX_PACKET];
		memset(icmp_data,0,MAX_PACKET);
		int datasize=DEF_PACKET_SIZE;
		datasize+=sizeof(IcmpHeader);
		fill_icmp_data(icmp_data,datasize);
		((IcmpHeader*)icmp_data)->checksum=0;
		((IcmpHeader*)icmp_data)->seg=0;
	((IcmpHeader*)icmp_data)->checksum=checksum((USHORT*)icmp_data,datasize);
		int bwrote=sendto(sockRaw,icmp_data,datasize,0,(struct sockaddr *)pIPAddrTemp,sizeof(dest));
		int n=0;
		if(bwrote==SOCKET_ERROR)
		{
			if(WSAGetLastError()==WSAETIMEDOUT)
			{
				std::cout<<"timed out"<<std::endl;
			}
			std::cout<<"sendto failies"<<WSAGetLastError()<<std::endl;
			ExitProcess(STATUS_FAILED);
			n=1;

		}
		if(WSAGetLastError()==WSAETIMEDOUT)
		{
			std::cout<<"timed out"<<std::endl;
			ExitProcess(STATUS_FAILED);
			n=1;
		}
		if(bwrote<datasize)
		{
			std::cout<<"Worte"<<bwrote<<"bytes"<<std::endl;
			ExitProcess(STATUS_FAILED);
			n=1;
		}
		int bread=recvfrom(sockRaw,recvbuf,MAX_PING_PACKET_SIZE,0,(struct sockaddr *)&from,&fromlen);
		if(bread==SOCKET_ERROR)
		{
			if(WSAGetLastError()==WSAETIMEDOUT)
			{
				std::cout<<"timed out"<<std::endl;
			}
			std::cout<<"recvfrom falied"<<WSAGetLastError()<<std::endl;
			ExitProcess(STATUS_FAILED);
			n=1;
		}
		if(n==0)
		{
			decode_resp(recvbuf,bread,&from);
			InterlockedDecrement(aa);
		}
		return 0;
	}
