using namespace std;
#include <iostream>

#include <stdio.h>
#include <windows.h>
#include <shlobj.h>

#pragma comment(lib,"shell32.lib")

/* 获取桌面路径 */
BOOL GetDesktopPath(char *pszDesktopPath) {
    LPITEMIDLIST items = NULL;

    if(SHGetSpecialFolderLocation(NULL, CSIDL_DESKTOPDIRECTORY, &items) == S_OK) {
        BOOL flag = SHGetPathFromIDListA(items, pszDesktopPath);
        CoTaskMemFree(items);
        return flag;
    }

    return FALSE;
}

/* 创建桌面快捷方式 */
BOOL CreateFileShortcut(LPCSTR lpszFileName, LPCSTR lpszLnkFileDir, LPCSTR lpszLnkFileName, LPCSTR lpszWorkDir, WORD wHotkey, LPCWSTR lpszDescription, int iShowCmd) {
    /*
    函数功能：对指定文件在指定的目录下创建其快捷方式
    函数参数：
    lpszFileName    指定文件，为NULL表示当前进程的EXE文件。
    lpszLnkFileDir  指定目录，不能为NULL。
    lpszLnkFileName 快捷方式名称，为NULL表示EXE文件名
    lpszWorkDir     工作目录，一般设置为NULL
    wHotkey         为0表示不设置快捷键
    pszDescription  备注
    iShowCmd        运行方式，默认为常规窗口
    */
    HRESULT hRet;       //函数返回值类型
    IShellLink *pLink = NULL;  //指向接口实例的指针
	IPersistFile *ppf = NULL;
    char szBuffer[MAX_PATH];
    WCHAR wsz[MAX_PATH];

    if(lpszLnkFileDir == NULL) {
        cout << "指定快捷方式生成的目录为空！";
        return FALSE;
    }

    //通过调用实例的QueryInterface()方法我们又创建了一个COM接口的实例
    //不过这个实例隶属于原来的pLink实例
    hRet = CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, IID_IShellLink, (void**)&pLink);

    if(FAILED(hRet)) {
        cout << "创建IShellLink实例失败";
        return FALSE;
    }

    hRet = pLink->QueryInterface(IID_IPersistFile, (void**)&ppf);   //从IShellLink对象中获取IPersistFile接口

    if(FAILED(hRet)) {
        pLink->Release();
        cout << "获取IPersistFile接口失败";
        return FALSE;
    }

    //创建之后就可以对pLink设置目标，快捷键，备注等
    //设置完后再用ppf创建快捷方式文件就可以了
    //最后要记得释放资源

	//_pgmptr表示当前可执行程序的全路径
    if(lpszFileName == NULL) {
        MultiByteToWideChar(CP_ACP, 0, _pgmptr, -1, wsz, MAX_PATH); //使用MultiByteToWideChar()函数转换为Unicode
        pLink->SetPath(wsz);
    } else {
        MultiByteToWideChar(CP_ACP, 0, lpszFileName, -1, wsz, MAX_PATH);
        pLink->SetPath(wsz);
    }

    if(lpszWorkDir != NULL) {
        MultiByteToWideChar(CP_ACP, 0, lpszWorkDir, -1, wsz, MAX_PATH);
        pLink->SetPath(wsz);
    }

    if(wHotkey != 0) {
        pLink->SetHotkey(wHotkey);
    }

    pLink->SetShowCmd(iShowCmd);    //Sets the show command for a Shell link object. The show command sets the initial show state of the window.
    pLink->SetDescription(lpszDescription);
    pLink->SetIconLocation(TEXT("c:\\windows\\explorer.exe"), 20);

	//获取可执行程序路径及盘符、目录、文件名、扩展名
    char   path_buffer[_MAX_PATH];
    char   drive[_MAX_DRIVE];
    char   dir[_MAX_DIR];
    char   fname[_MAX_FNAME];
    char   ext[_MAX_EXT];
    _splitpath(_pgmptr, drive, dir, fname, ext);

    if(lpszLnkFileName != NULL) {
        sprintf(szBuffer, "%s\\%s", lpszLnkFileDir, lpszLnkFileName);
    } else {
        sprintf(szBuffer, "%s\\%s%s", lpszLnkFileDir, fname, ".lnk");
    }

    MultiByteToWideChar(CP_ACP, 0, szBuffer, -1, wsz, MAX_PATH);
    hRet = ppf->Save(wsz, TRUE);    //实际保存到wsz指定的位置
    ppf->Release();                 //释放IShellLink对象
    pLink->Release();               //释放IShellLink对象
    return SUCCEEDED(hRet);
}

int main(int args, char* argv[]) {
    char szPath[MAX_PATH] = { 0 };
    CoInitialize(NULL); //初始化COM

    if(GetDesktopPath(szPath)) {
        printf("%s\n", szPath);

        if(CreateFileShortcut(argv[1], "D:\\Win+R", NULL, NULL, 0, TEXT("快捷方式描述"), SW_SHOWNORMAL)) {
            cout<<"ok!"<<endl;
        } else {
            cout<<"error!"<<endl;
        }
    }

    CoUninitialize();   //释放COM接口
	system("pause");
    return 0;
}