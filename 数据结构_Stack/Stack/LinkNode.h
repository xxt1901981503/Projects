/*
 * LinkNode.h
 *
 *  Created on: 2015��1��2��
 *      Author: Administrator
 */

#ifndef SRC_LINKNODE_H_
#define SRC_LINKNODE_H_

#include<iostream>
using namespace std;

template<class T>
class LinkNode {
//	friend class LinkStack<T> ;
public:
	LinkNode() :
			next(NULL) {
	}
	LinkNode(T data, LinkNode<T>* next) {
		this->data = data;
		this->next = next;
	}
	~LinkNode() {
	}
	void outPut() {
		cout << data << " ";
	}
//private:
	T data;
	LinkNode<T>* next;
};

#endif /* SRC_LINKNODE_H_ */
