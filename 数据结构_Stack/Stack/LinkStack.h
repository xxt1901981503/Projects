/*
 * LinkStack.h
 *
 *  Created on: 2015年1月2日
 *      Author: Administrator
 */

#ifndef SRC_LINKSTACK_H_
#define SRC_LINKSTACK_H_

#include <iostream>
using namespace std;

#include "LinkNode.h"

template<class T> class LinkStack;

template<class T>
class LinkStack {
public:
	LinkStack() :
			top(NULL) {
	}
	void push(const T& e);
	void pop();
	bool isEmpty();
	void outPut();
private:
	LinkNode<T> *top;
};

template<class T>
void LinkStack<T>::push(const T& e) {
	top = new LinkNode<T>(e, top);
}

template<class T>
bool LinkStack<T>::isEmpty() {
	return top == NULL;
}

template<class T>
void LinkStack<T>::pop() {
	if (isEmpty()) {
		cout << "栈为空！";
		return;
	}
	LinkNode<T>* tmpNode = top;
	top = top->next;
	delete tmpNode;
}

template<class T>
void LinkStack<T>::outPut() {
	if (isEmpty())
		cout << "栈为空！";
	LinkNode<T>* tmpNode = top;
	do {
		tmpNode->outPut();
		tmpNode = tmpNode->next;
	} while (tmpNode != NULL);
	delete tmpNode;
}

#endif /* SRC_LINKSTACK_H_ */
